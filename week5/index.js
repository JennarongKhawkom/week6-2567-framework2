const http = require("http");
const app = http.createServer(function (req, res) { 
    if(req.url === '/') {
        res.writeHead(200,{"Content-Type": "text/html"});
        res.write("<h3>Home page</h3>");
        res.end();
    } else if (req.url === "/about") {
        res.writeHead(200,{"Content-Type": "text/plain"});
        res.write("About page");
        res.end();
    } else if (req.url === "/admin") {
        res.writeHead(200,{"Content-Type": "text/html"});
        res.write("<h1>Admin page</h1>");
        res.end();
    } else{
        res.writeHead(200,{"Content-Type": "text/html"});
        res.write("<h1>404</h1>");
        res.end();

    }
});

const PORT = 3000;

app.listen(PORT,  () => {
    console.log(`Server running at http://localhost:${PORT}`);
});
